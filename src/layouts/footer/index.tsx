import React from 'react'
import { Row, Col, Container } from 'react-bootstrap'
import { useT } from '../../custom/hooks/useT';
import CallIcon from '@mui/icons-material/Call';
import TelegramIcon from '@mui/icons-material/Telegram';
import AlternateEmailIcon from '@mui/icons-material/AlternateEmail';
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded';
import "./style.scss";
import FooterColData from "./FooterCol/footerColData.json"
import FooterCol from './FooterCol';

function Footer() {
  const { t, lang } = useT()
  return (
    <footer className='footer'>
      <Container>
        <div className="footer_top">
          <Row>
            {
              FooterColData.map(data => (
                <Col key={data.id} md={3} sm={6}>
                  <FooterCol id={data.id} text={data.text[lang]} />
                </Col>
              ))
            }
          </Row>
        </div>
        <div className="footer_body">
          <Row>
            <Col lg={6} md={6} sm={12}>
              <div className="footer_about_us">
                <h4 className="footer_title">
                  {t("infos")}
                </h4>
                <p className="footer_content">
                  {t("infosText")}
                </p>
              </div>
            </Col>
            <Col lg={6} md={6} sm={12}>
              <div className="footer_contacts">
                <div>

                  <h4 className="footer_title">
                    {t("ourContacts")}
                  </h4>
                  <ul>
                    <li>
                      <a
                        href="tel:+90-536-487-07-80"
                      >
                        <CallIcon sx={{ fill: "#fff" }} /> Tel. +90-536-487-07-80
                      </a>
                    </li>
                    <li>
                      <a
                        href="mailto:fikratorg@gmail.com"
                      >
                        <AlternateEmailIcon sx={{ fill: "#fff" }} /> fikratorg@gmail.com
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.facebook.com/Fikrat-Nashriyoti-104849718806204"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <FacebookRoundedIcon sx={{ fill: "#fff" }} /> Fikrat Nashriyoti
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://t.me/fikratkitob"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <TelegramIcon sx={{ fill: "#fff" }} /> FIKRAT.ORG
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </Col>
            {/* <Col lg={4} md={6} sm={12}>
              <div className="footer_social_medias">
                <h4 className="footer_title">
                  {t("ourSupport")}
                </h4>

              </div>
            </Col> */}
          </Row>
        </div>
      </Container>
      <div className="footer_bottom">
        <Container fluid >
          <Row>
            <Col xs={6}>
              <div className="left">
                <p className="footer_content text-dark">
                  {t("allLowProtected")}
                </p>
              </div>
            </Col>

            <Col xs={6}>
              <div className="right">
                <p className="footer_content text-dark">
                  Powered by <a href="http://ifraganus.com" target="_blank" rel="noopener noreferrer">

                    <img src="/assets/img/ifraganus_logo.png" alt="ifraganus_logo" />

                  </a>
                </p>
              </div>
            </Col>

          </Row>
        </Container>
      </div>
    </footer>
  )
}

export default Footer