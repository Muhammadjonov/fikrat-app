import React from 'react'
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import ScreenShareIcon from '@mui/icons-material/ScreenShare';
import MoneyOffIcon from '@mui/icons-material/MoneyOff';
import AlarmOffIcon from '@mui/icons-material/AlarmOff';

interface IFooterCol {
  text: string;
  id: number
}

function FooterCol(props: IFooterCol) {

  const { id, text } = props;
  // svg icon ni tekshirish
  const svgIcon = id === 1 ? <AlarmOffIcon sx={{ fill: "#006D7C" }} /> : id === 2 ? <CloudDownloadIcon sx={{ fill: "#006D7C" }} /> : id === 3 ? <MoneyOffIcon sx={{ fill: "#006D7C" }} /> : id === 4 ? <ScreenShareIcon sx={{ fill: "#006D7C" }} /> : null

  return (
    <div className='footer_col'>
      {svgIcon}
      <p className='footer_content color_till'>{text}</p>
    </div>
  )
}

export default FooterCol