import React from 'react'
import HeaderTop from './HeaderTop'
import NavSec from "./navsec"
import { css } from "@emotion/css"

function Header() {
  return (
    <header className={css`
      background-color: #fff;
    
    `}>
      <HeaderTop />
      <NavSec />
    </header>
  )
}

export default Header