import React, { useState, useEffect } from 'react'
import { Col, Container, Dropdown, DropdownButton, Row } from 'react-bootstrap'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import "./style.scss";
import SearchIcon from '@mui/icons-material/Search';
import { changeLang, LangType, removeProbels, setLang, setActiveClass } from '../../../helpers';
import { useT } from '../../../custom/hooks/useT';
import useVal from '../../../custom/hooks/useVal';
import CustomSearchInput from '../../../custom/CustomSearchInput';

function HeaderTop() {
  const [isFocus, setIsFocus] = useState(false);
  const [searchVal, setSearchVal] = useVal("");
  let navigate = useNavigate();

  // path name o'zgarganda search valueni clear qilish
  let { pathname } = useLocation();
  useEffect(() => {
    if (pathname !== "/search") {
      setSearchVal({ currentTarget: { value: "" } })
    }
  }, [pathname])

  const search = (e: React.MouseEvent<HTMLButtonElement> | string) => {
    let searchableVal = '';
    if (typeof e === 'string') {
      searchableVal = e
    } else {
      e.preventDefault();
      searchableVal = searchVal
    }
    searchableVal = searchableVal.trim();
    searchableVal = removeProbels(searchableVal);
    console.log("search", searchableVal);
    if (searchableVal && searchableVal.length > 2) {
      navigate(`/search?key=${searchableVal}`);
    }
  };

  useEffect(() => {
    const search_input = document.getElementById("search_input")!;

    document.body.addEventListener("click", function (e: MouseEvent) {
      const clickedPlace = e.target as Node;

      if (!search_input?.contains(clickedPlace)) {
        setIsFocus(false);
      }
    });
  }, []);
  // tilni o'zgartirish

  const { lang } = useT();
  const langTitle = lang === "uz" ? "O'zbekcha" : "Ўзбекча";

  const handleSetLang = (language: LangType) => {
    setLang(language);
    changeLang(language);
    if (lang !== language) {
      window.location.reload();
    }
  }

  const handleActiveClass = () => setActiveClass("");

  return (
    <Container fluid>
      <div className="header_top">
        <Row>
          <Col md={2} sm={6}>
            <div className="logo_wrapper">
              <Link onClick={handleActiveClass} className='logo_brand' to={"/"}>
                <figure>
                  <img src="/assets/img/fikrat_logo.svg" alt="logo" />
                </figure>
              </Link>
            </div>
          </Col>
          <Col md={8} sm={6}>
            <div className="search_form">
              <CustomSearchInput searchVal={searchVal} setSearchVal={setSearchVal} submit={search} />
              {/* <Form>
                <Form.Group controlId="formBasicSearch">
                  <div id="search_input" className="search_input_wrapper">
                    <Form.Control className={isFocus ? "focus" : ""} type="search" placeholder="Izlash..." />
                    <span className="search_icon" onClick={() => setIsFocus(true)}>
                      <SearchIcon sx={{ fill: "#006D7C" }} />
                    </span>
                  </div>
                </Form.Group>
              </Form> */}
            </div>
          </Col>
          <Col md={2} sm={12}>
            <div className="language_wrapper">
              <DropdownButton align="end" title={langTitle} id="dropdown-menu-align-end">
                <Dropdown.Item
                  onClick={() => handleSetLang("uz")}
                  className={lang === "uz" ? "selected" : ""}
                  eventKey="1"
                >
                  O'zbekcha
                </Dropdown.Item>
                <Dropdown.Item
                  onClick={() => handleSetLang("en")}
                  className={lang === "en" ? "selected" : ""}
                  eventKey="2"
                >
                  Ўзбекча
                </Dropdown.Item>
              </DropdownButton>
            </div>
          </Col>
        </Row>
      </div>
    </Container>
  )
}

export default HeaderTop