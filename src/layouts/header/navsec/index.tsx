import React, { useState, useEffect } from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import NavLinkData from "./NavLinkBtn/navlinkData.json"
import NavLinkBtn from "./NavLinkBtn"
import HomeIcon from '@mui/icons-material/Home';
import { useT } from '../../../custom/hooks/useT';
import "./style.scss";
import { Link } from 'react-router-dom';
import { setActiveClass } from '../../../helpers';

function NavSec() {

  const [isExpanded, setIsExpanded] = useState(false)

  const setNavExpanded = (expended: boolean) => setIsExpanded(expended)

  const handleClick = (to: string) => {
    setIsExpanded(false);
    setActiveClass(to)
  }

  // navLink ni boshqa joy bosilganda yopish

  useEffect(() => {
    const basicNavbar = document.getElementById("basic-navbar-nav");
    const navbarBtn = document.getElementById("navbar_toggler");

    document.body.addEventListener("click", function (e: MouseEvent) {
      const clickedPlace = e.target as Node;

      if (!(basicNavbar?.contains(clickedPlace) || navbarBtn?.contains(clickedPlace))) {
        setIsExpanded(false);
      }
    });
  }, []);

  // const closeNav = () => setIsExpanded(false)

  const handleActiveClass = () => setActiveClass("");

  return (
    <div className="header_bottom">
      <Navbar expand="lg" expanded={isExpanded} onToggle={setNavExpanded} >
        <Container fluid>
          <Navbar.Brand href="#">
            <Link
              to={"/"}
              onClick={handleActiveClass}
            >
              <HomeIcon sx={{
                fill: "#006D7C"
              }} />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle id="navbar_toggler" aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav" className="">
            <Nav>
              {
                NavLinkData.map((data, idx) => (
                  <NavLinkBtn
                    onClick={handleClick}
                    key={data.id}
                    text={data.text}
                    hrefUrl={data.hrefUrl}
                  />
                ))
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}

export default NavSec