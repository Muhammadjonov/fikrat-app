import React from 'react'
import { Link, useLocation } from "react-router-dom";
import { useT } from '../../../../custom/hooks/useT'


type NavLinkType = {
  text: string,
  hrefUrl: string,
  onClick: (to: string) => void,
}

function NavLinkBtn(props: NavLinkType) {
  const { text, hrefUrl, onClick } = props;
  const { t } = useT();
  const { pathname } = useLocation()
  const activeClass = pathname === hrefUrl ? "active" : "";


  return (
    <Link className={`nav_link category ${activeClass}`}
      to={hrefUrl}
      onClick={() => onClick(hrefUrl)}
    >
      {t(text)}
    </Link>
  )
}

export default NavLinkBtn