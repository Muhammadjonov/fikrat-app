import React, { useEffect, createContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Footer from './layouts/footer';
import Header from './layouts/header';
import { Route, Routes, useLocation } from 'react-router-dom';
import { fallbackLng, languages } from './constants';
import { routes } from './routes';
import { RouteType } from './types';

type ActiveClassContextType = {
  setActiveClass: React.Dispatch<React.SetStateAction<string>>,
  activeClass: string
}
export const ActiveClassContext = createContext<ActiveClassContextType>({} as ActiveClassContextType)

function App() {
  const { pathname } = useLocation()
  const { i18n } = useTranslation();
  const [activeClass, setActiveClass] = useState<string>("");


  const contextValue = {
    setActiveClass,
    activeClass
  }

  useEffect(() => {
    let currentLang = localStorage.getItem("lang");

    if (!currentLang) {
      localStorage.setItem("lang", fallbackLng);
    } else if (languages.includes(currentLang)) {
      i18n.changeLanguage(currentLang);
    };


  }, [i18n]);

  // pathname o'zgarganda tepaga otish
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }, [pathname]);

  return (
    <div id="fikrat_wrapper">
      <ActiveClassContext.Provider value={contextValue}>

        <Header />
        <Routes>
          {
            routes.map((route: RouteType, idx) => {
              return <Route key={idx} path={route.path} element={<route.element />} />;
            })
          }
        </Routes>
        <Footer />
      </ActiveClassContext.Provider>
    </div >
  )
}

export default App;
