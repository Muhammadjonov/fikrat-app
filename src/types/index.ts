
export type RouteType = {
  path: string,
  element: React.LazyExoticComponent<() => JSX.Element>
}



export type LanguageType = {
  id: string,
  name: string
}

export type LanguagesType = LanguageType[]

export type CategoryType = {
  id: string,
  name: string
}

export type CategoriesType = CategoryType[];

export type AuthorType = {
  id: string,
  full_name: string
}

export type AuthorsType = AuthorType[]

export type BookDetailType = {
  id?: number,
  title?: string,
  publisher?: string,
  description?: string,
  category?: CategoryType,
  language?: LanguageType,
  author?: AuthorType,
  year?: number,
  number_of_pages?: number,
  image?: string,
  is_audio: boolean,
  file?: string,
  file_size?: string,
  file_extension?: string
}

export type WiseWordsType = {
  count: number,
  links: {
    next: string | null,
    previous: string | null
  },
  num_pages: number,
  results: { image: string }[]
}

export type GaleriesType = {
  count: number,
  links: {
    next: string | null,
    previous: string | null
  },
  num_pages: number,
  results: { image: string }[]
}

export type BooksWorldType = {
  count: number,
  links: {
    next: string | null,
    previous: string | null
  },
  num_pages: number,
  results: {
    id: number, title: string, publisher: string, description: string, author: {
      id: string,
      full_name: string
    },
    image: string
  }[]
}

export type AudioBooksType = {
  count: number,
  links: {
    next: string | null,
    previous: string | null
  },
  num_pages: number,
  results: {
    id: number, title: string, publisher: string, description: string, author: {
      id: string,
      full_name: string
    },
    image: string
  }[]
}

export type MediasType = {
  count: number,
  links: {
    next: string | null,
    previous: string | null
  },
  num_pages: number,
  results: {
    id: number,
    title: string,
    video: string,
    created_at: string
  }[]
}

export type ArticlesType = {
  count: number,
  links: {
    next: string | null,
    previous: string | null
  },
  num_pages: number,
  results: {
    id: number,
    title: string,
    content: string,
    category: {
      id: number,
      name: string
    },
    language: {
      id: number,
      name: string
    },
    author: AuthorType,
    created_at: string
  }[]
}