import i18n from "i18next";
import { fallbackLng } from './../constants/index';
export type LangType = "uz" | "en";

export const getLang = (): LangType => {
  const lang = localStorage.getItem("lang");
  if (lang === "uz" || lang === "en") return lang;
  return fallbackLng;
}

export const setLang = (lang: LangType) => {
  localStorage.setItem("lang", lang)
}

export const changeLang = (lang: LangType) => {
  i18n.changeLanguage(lang);
}

export const ignoreFalsyValues = (obj: any) => {
  Object.keys(obj).map(key => {
    if (!obj[key]) {
      delete obj[key]
    }
  })
  return obj
}

export const removeProbels = (str: string) => {
  let re = /\s + /g;
  str = str.replace(re, " ");
  return str;
}

export const setActiveClass = (className: string | "") => {
  localStorage.setItem("activeClassName", className)
}

export const getActiveClass = () => {
  return localStorage.getItem("activeClassName");
}