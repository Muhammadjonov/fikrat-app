import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import Loading from './components/Loading';
import reportWebVitals from './reportWebVitals';
import 'bootstrap/dist/css/bootstrap.min.css';
// Import Swiper styles
import "swiper/css";
import "swiper/css/navigation";
import "./i18next/config"
import {
  BrowserRouter as Router,
} from "react-router-dom";
import "./styles/style.scss";
import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <Suspense fallback={<Loading height="100vh" />}>
      <Router>
        <App />
      </Router>
    </Suspense>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
