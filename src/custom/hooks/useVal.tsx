import React, { useState } from "react";

const useVal = (value: string): [string, any] => {
  const [val, setVal] = useState(value);

  const setValFunc = (e: React.FormEvent<HTMLInputElement>) => {
    setVal(e.currentTarget.value)
  }

  return [val, setValFunc];
};

export default useVal;