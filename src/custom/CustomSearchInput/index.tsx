import React from 'react'
import "./style.scss";

interface ICustomSearchInput {
  submit: (e: React.MouseEvent<HTMLButtonElement> | string) => void,
  searchVal: string,
  setSearchVal: () => void,
}

function CustomSearchInput(props: ICustomSearchInput) {
  const { submit, searchVal, setSearchVal } = props
  return (
    <form className="search-bar">
      <input
        className='search_input'
        type="search"
        value={searchVal}
        onChange={setSearchVal}
        name="search"
        required
        autoComplete='off'
      />
      <button className="search-btn" type="submit" onClick={submit}>
        <span>Search</span>
      </button>
    </form>
  )
}

export default CustomSearchInput