import React, { useCallback, useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { useParams } from 'react-router-dom';
import { bookUrl } from '../../api/apiUrls';
import baseAPI from '../../api/baseAPI';
import { useT } from '../../custom/hooks/useT';
import { BookDetailType } from '../../types';
import DownloadBtn from './DownloadBtn';
import "./style.scss";

function BookView() {
  const { t } = useT();
  const [bookDetails, setBookDetails] = useState<BookDetailType>();
  const { book_id } = useParams();

  const bookId = book_id?.slice(1);

  const getBookDetails = useCallback(() => {

    baseAPI
      .find<BookDetailType>(bookId, bookUrl)
      .then(res => {
        setBookDetails(res.data);

      })
      .catch(error => console.log("error", error));
  }, [bookId])

  useEffect(() => {
    getBookDetails();
  }, [getBookDetails])

  return (
    <section className="book_view">
      <Container>
        <Row>
          <Col lg={3}>
            <div className="left">
              <figure>
                <img src={bookDetails?.image} alt={bookDetails?.title} />
              </figure>
              <div className="button_area">
                <DownloadBtn file={bookDetails?.file} />
              </div>
            </div>
          </Col>
          <Col lg={9}>
            <div className="right">
              <h1 className="book_view_title">
                {bookDetails?.title}
              </h1>
              <p className="book_view_content">
                {bookDetails?.description}
              </p>

              <table className="book_desc">
                <tr>
                  <td className="book_view_content year">{t("year")}:</td>
                  <td className="book_view_content lang">{bookDetails?.year}</td>
                  <td className="book_view_content lang">{t("publisher")}:</td>
                  <td className="book_view_content">{bookDetails?.publisher}</td>
                </tr>
                <tr>
                  <td className="book_view_content year">{t("language")}:</td>
                  <td className="book_view_content lang">{bookDetails?.language?.name}</td>
                  <td className="book_view_content lang">{t("pages")}:</td>
                  <td className="book_view_content">{bookDetails?.number_of_pages}</td>
                </tr>
                <tr>
                  <td className="book_view_content year">{t("file")}:</td>
                  <td className="book_view_content lang" style={{ textTransform: "lowercase" }}>{`${bookDetails?.file_extension}, ${bookDetails?.file_size}`}</td>

                </tr>

              </table>

            </div>
          </Col>
        </Row>
      </Container>
    </section >
  )
}

export default BookView