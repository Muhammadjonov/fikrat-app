import React, { useState } from 'react'
import LoadingButton from '@mui/lab/LoadingButton';
import { useT } from '../../../custom/hooks/useT';
import FileDownloadIcon from '@mui/icons-material/FileDownload';

interface IDownload {
  file: string | undefined
}

function DownloadBtn(props: IDownload) {
  const { file } = props;
  const { t } = useT();
  return (
    <a className='download_btn' download={file} href={file} target="_blank" rel="noopener noreferrer">
      <LoadingButton
        color="primary"
        loadingPosition="start"
        startIcon={<FileDownloadIcon />}
        variant="contained"
      >
        {t("download")}
      </LoadingButton>
    </a>
  )
}

export default DownloadBtn;

