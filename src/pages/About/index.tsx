import React from 'react'
import { Col, Container, Row } from "react-bootstrap"
import { useT } from '../../custom/hooks/useT';
import "./style.scss";
function About() {
  const { t } = useT()
  return (
    <section className="about_us">
      <Container>
        <Row>
          <h2 className="title">
            {t("aboutUs")}
          </h2>
        </Row>
        <Row>
          <Col sm={12}>
            <div className="about">
              <h3 className="about_title">
                {t("aboutTitle")}
              </h3>
              <div className="about_body">
                <p className="category">
                  {t("aboutContent").split('\n').map(item => (
                    <>{item} <br /> <br /></>
                  ))}
                </p>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default About