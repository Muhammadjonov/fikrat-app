import React from 'react'
import { AuthorType } from '../../../types';

interface IArticleCard {
  title: string,
  content: string,
  author: AuthorType
}

function ArticleCard(props: IArticleCard) {
  const { title, content, author } = props;


  return (
    <div className="article_card">
      <h3 className="about_title">
        {author.full_name}<br />
        {title}
      </h3>
      <p className="atricle_content">
        {content}
      </p>
    </div>
  )
}

export default ArticleCard