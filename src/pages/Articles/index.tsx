import React, { useState, useEffect, useCallback } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { useT } from '../../custom/hooks/useT'
import ArticleCard from './ArticleCard'
import PaginationRounded from '../../components/PaginationRounded'
import "./style.scss";
import { ArticlesType } from '../../types'
import { articleUrl } from '../../api/apiUrls'
import baseAPI from '../../api/baseAPI'
import Loading from '../../components/Loading'

function Articles() {
  const { t } = useT();
  const [articles, setArticles] = useState<ArticlesType>();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState<number>(1);


  const handleChange = (event: any, value: number) => {
    setPage(value);
  };

  const getArticles = useCallback(() => {
    setLoading(true)
    baseAPI
      .fetchWithPagination<ArticlesType>({ url: articleUrl, page })
      .then(res => {
        setArticles(res.data);
        setLoading(false);
      })
      .catch(error => console.log("error", error))
  }, [page]);

  useEffect(() => {
    getArticles()
  }, [getArticles])

  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className='articles'>
      <Container>
        <Row>
          <h2 className="title">{t("articles")}</h2>
        </Row>
        <Row>
          <Col lg={12}>
            <Row>
              {
                loading ? (
                  <Loading height="100vh" />
                ) :
                  articles?.results.map((article, idx) => (
                    <Col key={idx} lg={12}>
                      <ArticleCard author={article.author} title={article.title} content={article.content} />
                    </Col>
                  ))
              }
            </Row>
            <Row>
              {
                articles && articles?.num_pages > 1 ?
                  (
                    <PaginationRounded handleChange={handleChange} page={page} count={articles?.num_pages} />
                  ) : null
              }
            </Row>
          </Col>
          {/* <Col lg={3}>
            <div className="filter_wrapper">
              <FilterCard title={categoryFilterData.title} checkboxes={categoryFilterData.checkboxes} />
            </div>
          </Col> */}
        </Row>
      </Container>
    </section>
  )
}

export default Articles