import React, { useCallback, useState, useEffect } from 'react'
import { Container, Col, Row } from 'react-bootstrap';
import BookCard from '../../components/BookCard';
// import FilterCard from '../../components/LanguagesFilterCard';
import AuthorsFilterCard from '../../components/AuthorsFilterCard';
import PaginationRounded from '../../components/PaginationRounded';
import { useT } from '../../custom/hooks/useT';
import "./style.scss";
import baseAPI from '../../api/baseAPI';
import { AuthorsType, BooksWorldType, CategoriesType, LanguagesType } from '../../types';
import { authorUrl, bookUrl, categoryUrl, languageUrl } from '../../api/apiUrls';
import { Link } from 'react-router-dom';
import CategoriesFilterCard from '../../components/CategoriesFilterCard';
import EmptySearchResult from '../EmptySearchResult';

export type SimpleObjectType = {
  [key: string]: any
}

function BooksWorld() {
  const { t } = useT();
  const [categories, setCategories] = useState<CategoriesType>();
  const [languages, setLanguages] = useState<LanguagesType>();
  const [authors, setAuthors] = useState<AuthorsType>();
  const [booksWorld, setBooksWorld] = useState<BooksWorldType>();
  const [page, setPage] = useState<number>(1);
  const [filters, setFilters] = useState<{ category?: SimpleObjectType, author?: SimpleObjectType, language?: SimpleObjectType }>({
    category: {},
    author: {},
    language: {}
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, value: number) => {
    setPage(value);
  };

  const getBooksWorld = useCallback((filter) => {
    console.log("filter", filter);

    const checkedCategories = Object.keys(filter['category']).filter(key => filter['category'][key]).join(",");

    const checkedLanguages = Object.keys(filter['language']).filter(key => filter['language'][key]).join(",");

    const checkedAuthors = Object.keys(filter['author']).filter(key => filter['author'][key]).join(",");

    baseAPI
      .fetchWithPagination<BooksWorldType>({ url: bookUrl, params: { category: checkedCategories, language: checkedLanguages, author: checkedAuthors }, page })
      .then(res => {
        setBooksWorld(res.data);
      })
  }, [page]);

  const getCategories = useCallback(() => {
    baseAPI
      .fetchAll<CategoriesType>(categoryUrl)
      .then(res => {
        setCategories(res.data);
      })
      .catch(error => console.log("error", error));
  }, [])

  const getLanguages = useCallback(() => {
    baseAPI
      .fetchAll<LanguagesType>(languageUrl)
      .then(res => {
        setLanguages(res.data);
      })
      .catch(error => console.log("error", error));
  }, [])

  const getAuthors = useCallback(() => {
    baseAPI
      .fetchAll<AuthorsType>(authorUrl)
      .then(res => {
        setAuthors(res.data);
      })
      .catch(error => console.log("error", error));
  }, [])

  useEffect(() => {
    getBooksWorld(filters);
  }, [getBooksWorld, filters])

  useEffect(() => {
    getCategories();
    getLanguages();
    getAuthors();
  }, [getCategories, getLanguages, getAuthors]);

  const handleChangeItemCheck = (name: 'category' | 'author' | 'language') => (obj: SimpleObjectType) => {
    setFilters(filter => ({
      ...filter,
      [name]: {
        ...(filter[name] || {}),
        ...obj
      }
    }))
  }

  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className="book_world">
      <Container>
        <Row>
          <h2 className="title">
            {t("booksWorld")}
          </h2>
        </Row>
        <Row className="book_world_row">
          <Col lg={9}>
            <div className="left">
              <Row>
                {
                  booksWorld?.results.length !== 0 ?
                    booksWorld?.results.map((book, idx) => (
                      <Col key={idx} lg={3} md={4} sm={6}>
                        <Link to={`/booksWorld/:${book.id}`}>

                          <BookCard image={book.image} author={book.author} title={book.title} />
                        </Link>
                      </Col>
                    )) : (
                      <EmptySearchResult isButton={false} searchKey="" />
                    )
                }
              </Row>
            </div>
            <Row>
              {
                booksWorld && booksWorld?.num_pages > 1 ?
                  (
                    <PaginationRounded handleChange={handleChange} page={page} count={booksWorld?.num_pages} />
                  ) : null
              }
            </Row>
          </Col>
          <Col lg={3}>
            <div className="filter_card_wrapper">
              <CategoriesFilterCard
                onCategoryChange={handleChangeItemCheck('category')}
                className="sections"
                title="sections"
                checkboxes={categories}
              />
              <CategoriesFilterCard
                onCategoryChange={handleChangeItemCheck('language')}
                className="languages"
                title={"languages"}
                checkboxes={languages}
              />
              <AuthorsFilterCard
                onCategoryChange={handleChangeItemCheck('author')}
                className="languages"
                title={"authors"}
                checkboxes={authors}
              />

            </div>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default BooksWorld