import React, { useCallback, useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { Link, useLocation } from "react-router-dom"
import baseAPI from '../../api/baseAPI';
import { allBookUrl } from '../../api/apiUrls';
import { BooksWorldType } from "../../types"
import PaginationRounded from '../../components/PaginationRounded';
import BookCard from '../../components/BookCard';
import "./style.scss";
import EmptySearchResult from '../EmptySearchResult';

function SearchResultPage() {
  const [searchResultBooks, setSearchResultBooks] = useState<BooksWorldType>();
  const [page, setPage] = useState<number>(1);
  const location = useLocation();
  let { search } = location;
  search = search.slice(5).replaceAll("%20", " ");
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, value: number) => {
    setPage(value);
  };

  const getSearchResultBooks = useCallback(() => {
    baseAPI
      .fetchWithPagination<BooksWorldType>({ url: allBookUrl, params: { search }, page })
      .then(res => {
        setSearchResultBooks(res.data)
      })
      .catch(error => console.log("error", error))
  }, [search, page])

  useEffect(() => {
    getSearchResultBooks()
  }, [getSearchResultBooks])

  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className="search_result_wrapper">
      <Container>
        {
          searchResultBooks?.results.length !== 0 ? (
            <>
              <Row>
                {
                  searchResultBooks?.results?.map((searchResultBook, idx) => (
                    <Col key={idx} lg={3} md={4} sm={6}>
                      <Link to={`/search/:${searchResultBook.id}`}>
                        <BookCard image={searchResultBook.image} author={searchResultBook.author} title={searchResultBook.title} />
                      </Link>
                    </Col>
                  ))
                }
              </Row>
              <Row>
                {
                  searchResultBooks && searchResultBooks?.num_pages > 1 ?
                    (
                      <PaginationRounded handleChange={handleChange} page={page} count={searchResultBooks?.num_pages} />
                    ) : null
                }
              </Row>
            </>
          ) : (
            <EmptySearchResult isButton={true} searchKey={search} />
          )
        }

      </Container>
    </section>
  )
}

export default SearchResultPage;