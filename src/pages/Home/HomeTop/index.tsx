import React from 'react'
import { Link } from 'react-router-dom';
import { Col, Row, Container } from "react-bootstrap"
import { useT } from '../../../custom/hooks/useT';
import DetailBtn from '../DetailBtn';
import ReactPlayer from 'react-player/lazy';
import Loading from "../../../components/Loading";

function HomeTop() {
  const { t } = useT();

  return (
    <div className='home_top'>
      <Container>
        <Row>
          <h1 className="home_title">
            {t("welcome")}
          </h1>
          <p className="home_info">
            {t("homeInfo")}
          </p>
        </Row>
        <Row>
          <Col md={7}>
            <div className="left">

              <ReactPlayer fallback={<Loading height="400px" />} controls url="http://www.youtube.com/embed/W7qWa52k-nE" width="100%" height="100%" />

              {/* <figure>
                    <img src="/assets/img/no_internet_video.png" alt="no internet" />
                  </figure> */}


            </div>
          </Col>
          <Col md={5}>
            <div className="right">
              <figure>
                <img width="100%" src="/assets/img/home_image1.png" alt="Fikrat" />
              </figure>

              <h4 className="home_about_title">
                {t("aboutUs")}
              </h4>
              <p className="home_about_info">
                {t("homeAboutInfo")}
              </p>
              <div className="button_area">
                <DetailBtn to="/about" />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default HomeTop