import React, { useCallback, useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import DetailBtn from '../DetailBtn';
import { useT } from "../../../custom/hooks/useT";
import { BooksWorldType } from '../../../types';
import baseAPI from '../../../api/baseAPI';
import { bookUrl } from '../../../api/apiUrls';
import Loading from '../../../components/Loading';
import { Link } from "react-router-dom";

function HomeBottom() {
  const [loading, setLoading] = useState<boolean>(false)
  const { t } = useT();
  const [homeBooks, setHomeBooks] = useState<BooksWorldType>();

  const getHomeBooks = useCallback(async () => {
    setLoading(true);
    baseAPI
      .fetchWithPagination<BooksWorldType>({ url: bookUrl })
      .then(res => {
        setHomeBooks(res.data);
        setLoading(false)
      })
      .catch(error => console.error(error))
  }, [])

  useEffect(() => {
    getHomeBooks()
  }, [getHomeBooks])

  return (
    <div className="home_bottom">
      <Container >
        <Row>
          <h2 className="title">
            {t("booksWorld")}
          </h2>
        </Row>
        <Row>
          {
            loading ? (
              <Loading height="400px" />
            ) :
              homeBooks?.results.map((book, idx) => {
                if (idx < 4) {
                  return (
                    <Col key={idx} lg={3} md={6}>
                      <Link to={`/:${book.id}`}>
                        <div className="image_card">
                          <figure>
                            <img src={book.image} alt={book.title} />
                          </figure>
                        </div>
                      </Link>
                    </Col>
                  )
                }
                return null
              }

              )
          }
        </Row>
        <Row>
          <div className="button_area">
            <DetailBtn to="/booksWorld" />
          </div>
        </Row>
      </Container>
    </div>
  )
}

export default HomeBottom