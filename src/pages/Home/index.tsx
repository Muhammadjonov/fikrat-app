
import React, { useState } from 'react'
import HomeBottom from './HomeBottom';
import HomeTop from './HomeTop';


import "./style.scss";

function Home() {

  const [active, setActive] = useState("booksWorld");


  const changeActiveClass = (className: string) => {
    setActive(className);
  }

  return (
    <section className='home'>
      <HomeTop />
      <HomeBottom />
    </section>
  )
}

export default Home