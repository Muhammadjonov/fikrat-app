import React from 'react'
import { Link } from 'react-router-dom'
import { useT } from '../../../custom/hooks/useT';
import "./style.scss"

interface IDetailBtn {
  to: string
}

function DetailBtn(props: IDetailBtn) {
  const { t } = useT()
  const { to } = props;
  return (
    <Link className="detail_btn" to={to}>
      {t("detail")}
    </Link>
  )
}

export default DetailBtn