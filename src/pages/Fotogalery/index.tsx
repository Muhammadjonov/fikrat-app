import React, { useState, useEffect, useCallback } from 'react'
import { Container, Row, Col } from 'react-bootstrap';
import { useT } from '../../custom/hooks/useT';
import PaginationRounded from '../../components/PaginationRounded';
import "./style.scss";
import { GaleriesType } from '../../types';
import { photoUrl } from '../../api/apiUrls';
import baseAPI from '../../api/baseAPI';
import Loading from '../../components/Loading';

function Fotogalery() {
  const { t } = useT();

  const [loading, setLoading] = useState(false);
  const [galeries, setGaleries] = useState<GaleriesType>();
  const [page, setPage] = useState<number>(1);

  const handleChange = (event: any, value: number) => {
    setPage(value);
  };

  const getGaleries = useCallback(async () => {
    setLoading(true);
    baseAPI.fetchWithPagination<GaleriesType>({ url: photoUrl, page })
      .then(res => {
        setGaleries(res.data)
        setLoading(false)
      })
      .catch(error => console.log("error", error))
  }, [page])

  useEffect(() => {
    getGaleries()
  }, [getGaleries])

  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className="fotogalery">
      <Container>
        <Row>
          <h2 className="title">
            {t("fotogalery")}
          </h2>
        </Row>
        <Row>
          {
            loading ? (
              <Loading height="400px" />
            ) : galeries?.results.map((galery, idx) => {
              if (idx == 0 || idx == 2 || idx == 5 || idx == 7) {
                return (
                  <Col lg={4} md={4} sm={6}>
                    <div className="foto_card">
                      <figure>
                        <img src={galery.image} alt="Galeriya" />
                      </figure>
                    </div>
                  </Col>
                )
              }
              return (
                <Col lg={2} md={4} sm={6}>
                  <div className="foto_card">
                    <figure>
                      <img src={galery.image} alt="Galeriya" />
                    </figure>
                  </div>
                </Col>
              )
            })
          }
        </Row>
        <Row>
          {
            galeries && galeries?.num_pages > 1 ? (
              <PaginationRounded handleChange={handleChange} page={page} count={galeries?.num_pages} />
            ) : null
          }
        </Row>
      </Container>
    </section>
  )
}

export default Fotogalery