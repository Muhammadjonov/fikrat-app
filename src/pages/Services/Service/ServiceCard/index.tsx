import React from 'react'
import { useT } from '../../../../custom/hooks/useT';

export interface IServiceCard {
  name: { "en": string, "uz": string },
  imgUrl: string,
  desc: { "en": string, "uz": string }
}

function ServiceCard(props: IServiceCard) {
  const { name, imgUrl, desc } = props;
  const { lang } = useT();
  return (
    <div className="service_card">
      <figure>
        <img src={imgUrl} width="100%" alt={name[lang]} />
      </figure>
      <h4 className="footer_title color_till text-center">
        {name[lang]}
      </h4>
      {/* <p className="service_card_desc">
        {desc[lang]}
      </p> */}
    </div>
  )
}

export default ServiceCard