import React from 'react'
import { Col, Row } from 'react-bootstrap'
import { SwiperSlide, Swiper } from 'swiper/react';
import ServiceCard from './ServiceCard';
import { IServiceCard } from "./ServiceCard"

import { Navigation } from "swiper";

interface IServices {
  title: string,
  services: IServiceCard[]
}

function Service(props: IServices) {
  const { title, services } = props;
  return (
    <div className="service">
      <Row>
        <h3 className="services_title">
          {title}
        </h3>
      </Row>
      <Row>
        <div className="silder_wrapper">
          <Swiper
            slidesPerView={1}
            spaceBetween={10}
            navigation={true}
            modules={[Navigation]}
            breakpoints={{
              640: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
              768: {
                slidesPerView: 3,
                spaceBetween: 20,
              },
              1024: {
                slidesPerView: 4,
                spaceBetween: 30,
              },
            }}
            className="mySwiper"
          >

            {
              services.map((service, idx) => (
                <SwiperSlide>
                  <ServiceCard name={service.name} imgUrl={service.imgUrl} desc={service.desc} />
                </SwiperSlide>
                // <Col key={idx} lg={3} md={4} sm={6}>
                // </Col>

              ))
            }
          </Swiper>
        </div>
      </Row>
    </div>
  )
}

export default Service