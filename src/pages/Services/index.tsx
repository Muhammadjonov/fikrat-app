import React from 'react'
import { Container } from 'react-bootstrap';
import { useT } from '../../custom/hooks/useT';
import Home from '../Home';
import Service from './Service';
import "./style.scss";

const servicesData = [
  {
    title: {
      "uz": "Kitobga doir ishlar:",
      "en": "Китобга доир ишлар:"
    },
    services: [
      {
        name: {
          "uz": "Tarjima",
          "en": "Таржима"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Tahrir",
          "en": "Таҳрир"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Mutaxassislik xizmati",
          "en": "Мутахассислик хизмати"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Sahifalash",
          "en": "Саҳифалаш"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Muqova (dizayn)",
          "en": "Муқова (дизайн)"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
    ]
  },
  {
    title: {
      "uz": "Broshyura va reklamalar:",
      "en": "Брошюра ва рекламалар:"
    },
    services: [
      {
        name: {
          "uz": "Tanitim broshyuralari",
          "en": "Танитим брошюралари"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Reklama broshyralari",
          "en": "Реклама брошйралари"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Kart vizitlar",
          "en": "Карт визитлар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Kataloglar",
          "en": "Каталоглар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Logotip",
          "en": "Логотип"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Taklifnomalar",
          "en": "Таклифномалар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
    ]
  },
  {
    title: {
      "uz": "Shirkatingiz ismi va logosi tushirilgan:",
      "en": "Ширкатингиз исми ва логоси туширилган:"
    },
    services: [
      {
        name: {
          "uz": "Kundaliklar",
          "en": "Кундаликлар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Blaknotlar",
          "en": "Блакнотлар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Ruchka va qalamlar",
          "en": "Ручка ва қаламлар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Daftarlar va kichik not. daftarlar",
          "en": "Дафтарлар ва кичик нот. дафтарлар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
      {
        name: {
          "uz": "Chashka va suvenir idishlar",
          "en": "Чашка ва сувенир идишлар"
        },
        imgUrl: "/assets/img/books/duo_zikr.png",
        desc: {
          "uz": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Enim, sed laoreet magnis ut magna. Donec praesent.",
          "en": "Лорем ипсум долор сит амет, cонсеcтетур адиписcинг элит. Эним, сед лаореэт магнис ут магна. Донеc праэсент."
        }
      },
    ]
  },
]

function Services() {
  const { lang } = useT()
  return (
    <section className="services">
      <Container>
        {
          servicesData.map((service, idx) => (
            <Service key={idx} title={service.title[lang]} services={service.services} />
          ))
        }
      </Container>
      <Home />
    </section>
  )
}

export default Services