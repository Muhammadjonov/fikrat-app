import React, { useCallback, useEffect, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
import { wiseWordUrl } from '../../api/apiUrls';
import baseAPI from '../../api/baseAPI';
import Loading from '../../components/Loading';
import PaginationRounded from '../../components/PaginationRounded';
import { useT } from "../../custom/hooks/useT";
import { WiseWordsType } from '../../types';
import "./style.scss";

function WiseWords() {
  const { t } = useT();
  const [loading, setLoading] = useState(false);
  const [wiseWords, setWiseWords] = useState<WiseWordsType>();
  const [page, setPage] = useState<number>(1);

  const handleChange = (event: any, value: number) => {
    setPage(value);
  };

  const getWiseWords = useCallback(async () => {
    setLoading(true);
    baseAPI.fetchWithPagination<WiseWordsType>({ url: wiseWordUrl, page })
      .then(res => {
        setWiseWords(res.data)
        setLoading(false)
      })
      .catch(error => console.log("error", error))
  }, [page])

  useEffect(() => {
    getWiseWords()
  }, [getWiseWords])


  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className="wise_words">
      <Container>
        <Row>
          <h2 className="title">
            {t("proverbPicture")}
          </h2>
        </Row>
        <Row>
          {
            loading ? (
              <Loading height="400px" />
            ) :
              wiseWords?.results?.map((result, idx) => (
                <Col key={idx} lg={3} md={4} sm={6}>
                  <div className="foto_card">
                    <figure>
                      <img src={result.image} alt="Rasmdagi hikmatlar" />
                    </figure>
                  </div>
                </Col>
              ))
          }
        </Row>
        <Row>
          {
            wiseWords && wiseWords?.num_pages > 1 ? (
              <PaginationRounded handleChange={handleChange} page={page} count={wiseWords?.num_pages} />
            ) : null
          }
        </Row>
      </Container>
    </section>
  )
}

export default WiseWords
