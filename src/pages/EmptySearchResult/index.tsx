import React from 'react'
import { Col, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useT } from '../../custom/hooks/useT'
import "./style.scss";

interface IEmptySearchResult {
  searchKey: string,
  isButton: boolean
}

function EmptySearchResult(props: IEmptySearchResult) {
  const { searchKey, isButton } = props;
  const { t, lang } = useT();

  let search_key = searchKey !== "" ? `"${searchKey}"` : null

  return (
    <div className="empty_search">
      <Row>
        <div className="image_area">
          <figure>
            <img src="/assets/img/search_result_not_found.png" alt="not found" />
          </figure>
        </div>
      </Row>
      <Row>
        <div className="content_area">
          <h2 className="title text-center text-dark">
            {t("noFound")} {search_key}
          </h2>
          <p className="atricle_content text-dark text-center">
            {t("pleaseTryAgain")}
          </p>
          {
            isButton ? (
              <div className="button_area">
                <Link to={"/"}>
                  {t("goBackToHome")}
                </Link>
              </div>
            ) : null
          }
        </div>
      </Row>
    </div>
  )
}

export default EmptySearchResult