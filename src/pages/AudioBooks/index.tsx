import React, { useCallback, useEffect, useState } from 'react'
import { Container, Col, Row } from 'react-bootstrap'
import { audioBookUrl, authorUrl, categoryUrl, languageUrl } from '../../api/apiUrls'
import baseAPI from '../../api/baseAPI'
import BookCard from '../../components/BookCard'
import { Link } from 'react-router-dom';
import PaginationRounded from '../../components/PaginationRounded'

import { useT } from '../../custom/hooks/useT'
import { AudioBooksType, AuthorsType, CategoriesType, LanguagesType } from '../../types'
import AuthorsFilterCard from '../../components/AuthorsFilterCard'
import CategoriesFilterCard from '../../components/CategoriesFilterCard'
import { SimpleObjectType } from '../BooksWorld'
import EmptySearchResult from '../EmptySearchResult'

function AudioBooks() {
  const { t } = useT();
  const [languages, setLanguages] = useState<LanguagesType>();
  const [audioBooks, setAudioBooks] = useState<AudioBooksType>();
  const [categories, setCategories] = useState<CategoriesType>();
  const [authors, setAuthors] = useState<AuthorsType>();
  const [filters, setFilters] = useState<{ category?: SimpleObjectType, author?: SimpleObjectType, language?: SimpleObjectType }>({
    category: {},
    author: {},
    language: {}
  });

  const [page, setPage] = useState<number>(1);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, value: number) => {
    setPage(value);
  };

  const getAudioBooks = useCallback((filter) => {

    const checkedCategories = Object.keys(filter['category']).filter(key => filter['category'][key]).join(",");

    const checkedLanguages = Object.keys(filter['language']).filter(key => filter['language'][key]).join(",");

    const checkedAuthors = Object.keys(filter['author']).filter(key => filter['author'][key]).join(",");

    baseAPI
      .fetchWithPagination<AudioBooksType>({ url: audioBookUrl, params: { category: checkedCategories, language: checkedLanguages, author: checkedAuthors }, page })
      .then(res => {
        setAudioBooks(res.data);
      })
  }, [page])

  const getLanguages = useCallback(() => {
    baseAPI.fetchAll<LanguagesType>(languageUrl)
      .then(res => {
        setLanguages(res.data)

      })
  }, [])

  const getCategories = useCallback(() => {
    baseAPI.fetchAll<CategoriesType>(categoryUrl)
      .then(res => {
        setCategories(res.data)

      })
  }, [])

  const getAuthors = useCallback(() => {
    baseAPI.fetchAll<AuthorsType>(authorUrl)
      .then(res => {
        setAuthors(res.data)

      })
  }, [])

  useEffect(() => {
    getAudioBooks(filters);
  }, [getAudioBooks, filters])

  useEffect(() => {
    getLanguages();
    getCategories();
    getAuthors();
  }, [getLanguages, getCategories, getAuthors]);


  const handleChangeItemCheck = (name: 'category' | 'author' | 'language') => (obj: SimpleObjectType) => {
    setFilters(filter => ({
      ...filter,
      [name]: {
        ...(filter[name] || {}),
        ...obj
      }
    }))
  }

  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className="book_world">
      <Container>
        <Row>
          <h2 className="title">
            {t("audioBooks")}
          </h2>
        </Row>
        <Row className="book_world_row">
          <Col lg={9}>
            <div className="left">
              <Row>
                {audioBooks?.results.length !== 0 ?
                  audioBooks?.results.map((book, idx) => (
                    <Col key={idx} lg={3} md={4} sm={6}>
                      <Link to={`/audioBooks/:${book.id}`}>
                        <BookCard image={book.image} author={book.author} title={book.title} />
                      </Link>
                    </Col>
                  )) : (
                    <EmptySearchResult isButton={false} searchKey="" />
                  )
                }
              </Row>
            </div>
            <Row>
              {
                audioBooks && audioBooks?.num_pages > 1 ?
                  (
                    <PaginationRounded handleChange={handleChange} page={page} count={audioBooks?.num_pages} />
                  ) : null
              }
            </Row>
          </Col>
          <Col lg={3}>
            <CategoriesFilterCard
              onCategoryChange={handleChangeItemCheck('category')}
              className="sections"
              title="sections"
              checkboxes={categories}
            />
            <CategoriesFilterCard
              onCategoryChange={handleChangeItemCheck('language')}
              className="languages"
              title={"languages"}
              checkboxes={languages}
            />
            <AuthorsFilterCard
              onCategoryChange={handleChangeItemCheck('author')}
              className="languages"
              title={"authors"}
              checkboxes={authors}
            />
          </Col>
        </Row>
      </Container>
    </section >
  )
}

export default AudioBooks