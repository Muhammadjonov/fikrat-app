import React, { useCallback, useState, useEffect } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { mediaUrl } from '../../api/apiUrls'
import baseAPI from '../../api/baseAPI'
import Loading from '../../components/Loading'
import PaginationRounded from '../../components/PaginationRounded'
import { useT } from '../../custom/hooks/useT'
import { MediasType } from '../../types'
import MediaCard from './MediaCard'
import "./style.scss";


function Media() {
  const { t } = useT();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState<number>(1);
  const [medias, setMedias] = useState<MediasType>();

  const handleChange = (event: any, value: number) => {
    setPage(value);
  };

  const getMedias = useCallback(() => {
    setLoading(true)
    baseAPI
      .fetchWithPagination<MediasType>({ url: mediaUrl, page })
      .then(res => {
        setMedias(res.data);
        setLoading(false);
      })
      .catch(error => console.log("error", error))
  }, [page]);

  useEffect(() => {
    getMedias()
  }, [getMedias]);

  // pagination almashganda tepaga otish 
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: "smooth"
    })
  }, [page])

  return (
    <section className="media">
      <Container>
        <Row>
          <h2 className="title">
            {t("media")}
          </h2>
        </Row>
        <Row>
          {
            loading ? (
              <Loading height="400px" />
            ) :
              medias?.results?.map((video, idx) => (
                <Col key={idx} lg={6}>
                  <MediaCard name={video.title} url={video.video} date={video.created_at} />
                </Col>
              ))
          }
        </Row>
        <Row>
          {
            medias && medias?.num_pages > 1 ? (
              <PaginationRounded handleChange={handleChange} page={page} count={medias?.num_pages} />
            ) : null
          }
        </Row>
      </Container>
    </section>
  )
}

export default Media