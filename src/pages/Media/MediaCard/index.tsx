import React from 'react'
import { Button } from '@mui/material'
import ReactPlayer from 'react-player';
import Loading from '../../../components/Loading';
import EventIcon from '@mui/icons-material/Event';

interface IMediaCard {
  name: string,
  url: string,
  date: string
}

function MediaCard(props: IMediaCard) {
  const { name, url, date } = props;

  const formatedDate = `${new Date(date).getHours().toString().padStart(2, '0')}:${new Date(date).getMinutes().toString().padStart(2, '0')} / ${(new Date(date).getMonth() + 1).toString().padStart(2, '0')}.${new Date(date).getDate().toString().padStart(2, '0')}.${new Date(date).getFullYear().toString().padStart(4, '0')}`;

  return (
    <div className="youtube_video_card">
      <div className="video_wrapper">
        <ReactPlayer fallback={<Loading height="400px" />} controls url={url} width="100%" height="100%" />
      </div>
      <div className="video_desc">
        <h3 className="about_title text-dark">
          {name}
        </h3>
        <Button className="date_btn" variant="contained">
          <EventIcon sx={{ fill: "#fff" }} /> {formatedDate}</Button>
      </div>
    </div>
  );
}

export default MediaCard;