import React from "react";
const BooksWorld = React.lazy(() => import("../pages/BooksWorld"));
const Home = React.lazy(() => import("../pages/Home"));
const About = React.lazy(() => import("../pages/About"));
const BookView = React.lazy(() => import("../pages/BookView"));
const AudioBooks = React.lazy(() => import("../pages/AudioBooks"));
const WiseWords = React.lazy(() => import("../pages/WiseWords"));
const Fotogalery = React.lazy(() => import("../pages/Fotogalery"));
const Media = React.lazy(() => import("../pages/Media"));
const Articles = React.lazy(() => import("../pages/Articles"));
const Services = React.lazy(() => import("../pages/Services"));
const SearchResultPage = React.lazy(() => import("../pages/SearchResultPage"));

export const routes = [
  {
    path: "/", element: Home
  },
  {
    path: "/:book_id", element: BookView
  },
  {
    path: "/about", element: About
  },
  {
    path: "/booksWorld", element: BooksWorld
  },
  {
    path: "/audioBooks", element: AudioBooks
  },
  {
    path: "/booksWorld/:book_id", element: BookView
  },
  {
    path: "/audioBooks/:book_id", element: BookView
  },
  {
    path: "/fotogalery", element: Fotogalery
  },
  {
    path: "/wiseWords", element: WiseWords
  },
  {
    path: "/media", element: Media
  },
  {
    path: "/articles", element: Articles
  },
  {
    path: "/services", element: Services
  },
  {
    path: "/services/:book_id", element: BookView
  },
  {
    path: "/search", element: SearchResultPage
  },
  {
    path: "/search/:book_id", element: BookView
  }
]