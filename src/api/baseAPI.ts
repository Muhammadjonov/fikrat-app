import { AxiosPromise } from "axios";
// import { PER_PAGE } from "../constants";
import { request } from "./config";

type FetchWithPaginationType = {
  url: string;
  page?: number;
  params?: object;
}

interface IBaseApi {
  find<T>(id: string | undefined, url: string, showNotification?: boolean): AxiosPromise<T>;
  fetchAll<T>(url: string, data?: object): AxiosPromise<T>;
  fetchWithParams<T>(url: string, params: object): AxiosPromise<T>;
  fetchWithPagination<T>(args: FetchWithPaginationType): AxiosPromise<T>;
}


const baseAPI: IBaseApi = {
  find<T>(id: string, url: string) {
    return request.get<T>(`${url}/${id}`);
  },
  async fetchAll<T>(url: string, data: object = {}) {
    const res = await request.get<T>(url, data,);
    return res;
  },
  async fetchWithParams<T>(url: string, params: object = {}) {
    const res = await request.get<T>(url, { params },);
    return res;
  },
  async fetchWithPagination<T>({
    url,
    page = 1,
    params = {},

  }: FetchWithPaginationType) {
    const res = await request.get<T>(url, {
      params: { ...params, page },
    });
    return res;
  }
}

export default baseAPI;