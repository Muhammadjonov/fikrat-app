import React from 'react'
import { Spinner } from 'react-bootstrap'
import { css } from '@emotion/css'

interface ILoading {
  height: string
}

function Loading(props: ILoading) {
  const { height } = props;
  return (
    <div className={css`
    display: flex;
    height: ${height};
    justify-content: center;
    align-items: center;
    background-color: #EDE2D1;
    `}>
      <Spinner animation="border" variant="primary" />

    </div>
  )
}

export default Loading