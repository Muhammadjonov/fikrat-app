import React from 'react'
import { Divider, FormControlLabel } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import { useT } from '../../custom/hooks/useT';
import "./style.scss";

interface ICategoriesFilterCard {
  title: string,
  checkboxes?: ICheckbox[],
  className?: string,
  onCategoryChange: (obj: any) => void
}

interface ICheckbox {
  id: string,
  name: string
}

function CategoriesFilterCard(props: ICategoriesFilterCard) {
  const { t } = useT()
  const { title, checkboxes, className = "", onCategoryChange } = props;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onCategoryChange({ [event.target.name]: event.target.checked })
  }

  return (
    <div className={`filter_card ${className}`}>
      <h4 className="footer_title color_till">
        {t(title)}
      </h4>
      <Divider />
      <FormGroup className={`filter_form ${className}`}>
        {
          checkboxes?.map((checkbox: ICheckbox, idx: any) => (
            <React.Fragment>
              <FormControlLabel
                control={
                  <Checkbox
                    onChange={handleChange}
                    name={checkbox.id}
                  />
                }
                label={checkbox.name}
              />
              <Divider />
            </React.Fragment>
          ))
        }
      </FormGroup>
    </div>
  )
}

export default CategoriesFilterCard