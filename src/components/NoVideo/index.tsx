import React from 'react'

function NoVideo() {
  return (
    <img src="/assets/img/no_internet_video.png" alt="no internet video" width="100%" height="100%" />
  )
}

export default NoVideo