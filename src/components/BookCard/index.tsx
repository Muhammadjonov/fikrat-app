import React from 'react'
import { Card } from 'react-bootstrap';
import "./style.scss"

interface IBookCard {
  image: string,
  author: {
    id: string,
    full_name: string
  }
  title: string
}

function BookCard(props: IBookCard) {
  const { image, author, title } = props;

  return (
    <div className="book_card">
      <Card style={{ width: '100%' }}>
        <Card.Img variant="top" src={image} />
        <Card.Body>
          <Card.Text>
            <p className="card_text">
              {author.full_name} :
            </p>
            <p className="card_text">
              {title}
            </p>
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  )
}

export default BookCard