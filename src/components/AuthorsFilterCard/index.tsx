import React from 'react'
import { Checkbox, Divider, FormControlLabel } from '@mui/material';
import { Form } from 'react-bootstrap';
import { useT } from '../../custom/hooks/useT';
import "./style.scss";
import { AuthorsType, AuthorType } from '../../types';

interface IAuthorsFilterCard {
  title: string,
  checkboxes?: AuthorsType,
  className?: string,
  onCategoryChange: (obj: any) => void

}


function AuthorsFilterCard(props: IAuthorsFilterCard) {
  const { t } = useT()
  const { title, checkboxes, className = "", onCategoryChange } = props;


  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onCategoryChange({ [event.target.name]: event.target.checked })
  };

  return (
    <div className={`filter_card ${className}`}>
      <h4 className="footer_title color_till">
        {t(title)}
      </h4>
      <Divider />
      <Form className={`filter_form ${className}`}>
        {
          checkboxes?.map((checkbox: AuthorType, idx: any) => (
            <React.Fragment key={idx}>
              <FormControlLabel
                control={
                  <Checkbox onChange={handleChange} name={checkbox.id} />
                }
                label={checkbox.full_name}
              />
              <Divider />
            </React.Fragment>
          ))
        }
      </Form>
    </div>
  )
}

export default AuthorsFilterCard