import * as React from 'react';
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';
import "./style.scss";

interface IPaginationRounded {
  count: number | undefined,
  handleChange: (event: any, value: number) => void,
  page: number
}

export default function PaginationRounded(props: IPaginationRounded) {
  const { count, handleChange, page } = props;
  return (
    <div className={"pagination_wrapper"}>
      <Stack spacing={2}>
        <Pagination onChange={handleChange} page={page} count={count} variant="outlined" shape="rounded" />
      </Stack>
    </div>
  );
}
