import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationUZ from "./uz/translation.json"
import translationEN from "./en/translation.json"
import { fallbackLng } from "../constants";
export const resources = {
  uz: { translation: translationUZ },
  en: { translation: translationEN }
}

i18n.use(
  initReactI18next
).init(
  {
    resources,
    lng: "uz",
    fallbackLng,
    interpolation: { escapeValue: false }
  }
)